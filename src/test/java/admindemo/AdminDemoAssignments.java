package admindemo;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.time.Duration;

import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import jxl.Sheet;
import jxl.Workbook;
import jxl.read.biff.BiffException;

public class AdminDemoAssignments {

	WebDriver driver;

	@BeforeClass
	public void beforeClass()
	{
		driver= new ChromeDriver();
		driver.get("https://admin-demo.nopcommerce.com/login");
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(10));
	}

	@Test(priority=0)
	public void Login() {
		
		WebElement emailid = driver.findElement(By.id("Email"));
		emailid.clear();
		emailid.sendKeys("admin@yourstore.com");
		WebElement password = driver.findElement(By.id("Password"));
		password.clear();
		password.sendKeys("admin");

		driver.findElement(By.xpath("//*[@class='button-1 login-button']")).click();

		String actual = driver.findElement(By.partialLinkText("John Smith")).getText();
		System.out.println(actual);
		String excepted = "John Smith";
		Assert.assertEquals(actual, excepted);
		driver.findElement(By.xpath("(//*[@class='nav-link'])[4]")).click();
	}


	@Test(priority=1)
	public void categories() throws InterruptedException, IOException, BiffException
	{
		driver.findElement(By.xpath("//p[contains(text(),' Categories')]")).click();
		driver.findElement(By.xpath("//i[@class='fas fa-plus-square']")).click();
		
		Thread.sleep(3000);

		File f=new File("src/test/resources/ExcelFile/assignmentselenium2.xls");
		FileInputStream fis= new FileInputStream(f);

		Workbook book=Workbook.getWorkbook(fis);
		Sheet sh=book.getSheet("Sheet1");

		int rowCount= sh.getRows();
		int columnsCount=sh.getColumns();
		for(int i=1; i<rowCount; i++)
		{
			String name= sh.getCell(0,1).getContents();

			String description= sh.getCell(1,1).getContents();

			String pricefrom= sh.getCell(2,1).getContents();

			String priceto= sh.getCell(3,1).getContents();

			String displayorder= sh.getCell(4,1).getContents();

			driver.findElement(By.xpath("//input[@name='Name']")).sendKeys(name);

			driver.findElement(By.id("tinymce")).sendKeys(description);

			JavascriptExecutor jsExecutor = (JavascriptExecutor) driver;
			jsExecutor.executeScript("document.getElementById('nestedSetting106772939').scrollIntoView()");

			driver.findElement(By.xpath("(//input[@role='spinbutton'])[3]")).sendKeys(pricefrom);

			driver.findElement(By.xpath("(//input[@role='spinbutton'])[5]")).sendKeys(priceto);

			driver.findElement(By.xpath("(//input[@role='spinbutton'])[7]")).sendKeys(displayorder);

		}

	}

	@Test(priority=2)
	public void Products() throws InterruptedException {

		driver.findElement(By.partialLinkText("Products")).click();
		driver.findElement(By.id("SearchProductName")).sendKeys("Build your own computer");
		WebElement testDropDown = driver.findElement(By.id("SearchCategoryId"));
		Select dropdown = new Select(testDropDown);
		dropdown.selectByIndex(2);
		driver.findElement(By.id("search-products")).click();

	}

	@Test(priority=3)
	public void Manufacturers() throws BiffException, IOException {

		driver.findElement(By.partialLinkText("Manufacturers")).click();
		driver.findElement(By.xpath("//a[@class='btn btn-primary']")).click();

		File f=new File("src/test/resources/ExcelFile/assignmentselenium2.xls");
		FileInputStream fis= new FileInputStream(f);

		Workbook book=Workbook.getWorkbook(fis);
		Sheet sh=book.getSheet("Sheet2");

		int rowCount= sh.getRows();
		int columnsCount=sh.getColumns();
		for(int i=1; i<rowCount; i++)
		{
			String name= sh.getCell(0,1).getContents();

			String description= sh.getCell(1,1).getContents();

			driver.findElement(By.id("SearchManufacturerName")).sendKeys(name);

			driver.findElement(By.id("SearchPublishedId")).sendKeys(description);

			driver.findElement(By.xpath("//button[@name='save']")).click();

		}

	}
	@Test(priority=4)
	public void Logout() {

		driver.findElement(By.xpath("//a[text()='Logout']")).click();
	}
	
	@AfterClass
	public void afterClass()
	{
		driver.quit();
	}
}
